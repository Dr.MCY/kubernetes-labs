# Kubernetes-Labs
-----------
[![Linux](https://svgshare.com/i/Zhy.svg)](https://svgshare.com/i/Zhy.svg) [![kubernetes](https://img.shields.io/badge/Kubernetes-Minikube-blue)](https://img.shields.io/badge/Kubernetes-Minikube-blue)


* Lab-1: Kubernetes (Minikube) installation on ubuntu AWS EC2
* Lab-2: First pod deployment
* Lab-3: Test of deployment
